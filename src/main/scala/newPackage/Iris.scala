package newPackage


//import statements
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

import scala.io.StdIn._


//object
object Iris {

  //main function
  def main(args: Array[String]): Unit = {

    //implemented to bypass log4j info logging to console
    Logger.getLogger("org").setLevel(Level.ERROR)


    // open Spark Session
    val spark: SparkSession = SparkSession
      .builder()
      .appName("IrisData")
      .master("local[*]")
      .getOrCreate()


    println("------------------------------------------------------------------------------------------")
    println(" Pulling separate DataFrames from one CSV file with with no header." +
      "\nIn this case we use the Iris Data set from 1936. Each DataFrame " +
      "\nwill have one class of fifty Irises each. All measurements are in cm.")


    import spark.implicits._

    //one set pulled from csv file
    val irisData = spark.read.format("csv")
      .load("iris.data")


    // Three DataFrames made from one data set
    val df1 = irisData.toDF("Sepal length", "Sepal width", "Petal length", "Petal width", "Class")
      .filter("Class = 'Iris-setosa'")
    val df2 = irisData.toDF("Sepal length", "Sepal width", "Petal length", "Petal width", "Class")
      .filter("Class = 'Iris-versicolor'")
    val df3 = irisData.toDF("Sepal length", "Sepal width", "Petal length", "Petal width", "Class")
      .filter("Class = 'Iris-virginica'")


    //query to discover if all frames will be handled the same
    print("\nWould you like to display the same amount of data in each frame? (y or n) ")

    val answer: Char = readChar()

    //sets the number of rows if user chose yes
    val inputA: Int =
      if (answer == 'y') {
        print("What number of rows would you like to show for each? ")
        readInt()
      }
      else {
        0 // had to set inputA in else otherwise code would not compile
      }

    //determines if number of rows for DF is already set
    val input1: Int =
      if (answer == 'y')
        inputA
      else {
        print("How many Setosa Irises would you like to see measurements? ")
        readInt()
      }
    println("\n\nThe first DataFrame will have columns named and return up to " + input1 + " Setosa Irises")
    df1.show(input1)
    if (input1 >= df1.count())
      println("Showing all " + df1.count() + " items")

    //determines if number of rows for DF is already set
    val input2: Int =
      if (answer == 'y')
        inputA
      else {
        print("How many Versicolor Irises would you like to see measurements? ")
        readInt()
      }
    println("\nThe second DataFrame will have columns named and return up to " + input2 + " Versicolor Irises")
    df2.show(input2)
    if (input2 >= df2.count())
      println("Showing all " + df2.count() + " items")

    //determines if number of rows for DF is already set
    val input3: Int =
      if (answer == 'y')
        inputA
      else {
        print("How many Virginica Irises would you like to see measurements? ")
        readInt()
      }
    println("\nThe third DataFrame will have columns named and return up to " + input3 + " Virginica Irises")
    df3.show(input3)
    if (input3 >= df3.count())
      println("Showing all " + df3.count() + " items")


    //The next section will allow user to choose a column and filter which data to view from it.
    println("\nThe next part will have to do with choosing data in the Iris set ---------------------------")

    val df = irisData.toDF("Sepal_length", "Sepal_width", "Petal_length", "Petal_width", "Class")
    //choice of column is first
    println("\n Which column would you like to use? ")
    print("(1)Sepal_length, (2)Sepal_width, (3)Petal_length, (4)Petal_width, (5)Class ")
    val numChoice = readInt()
    //pattern matching exercise to define column
    val choice: String = numChoice match {
      case 1 => "Sepal_length"
      case 2 => "Sepal_width"
      case 3 => "Petal_length"
      case 4 => "Petal_width"
      case _ => "Class"
    }
    //Column five is a one off with strings, so it gets handled separately
    if (choice == "Class") {
      println("Please choose which Class to view")
      print("(1)Iris-setosa ", "(2)Iris-versicolor ", "(3)Iris-virginica ")
      val numChoice2 = readInt()
      val classChoice: String = numChoice2 match {
        case 1 => "Class like 'Iris-set%'"
        case 2 => "Class like 'Iris-ver%'"
        case _ => "Class like 'Iris-vir%'"
      }
      df.filter(classChoice).show(50)
      println("Showing all fifty entries ")
    } else { // all other information is gathered here 
      print(" Please choose the measurement you like to look up? ")
      val measure = readLine()
      val newdf = df.filter(s"$choice like '$measure%'").count().toInt
      
      if (newdf > 0){
      df.filter(s"$choice like '$measure%'").sort(choice).show(newdf)
      println(s"Looking up all Irises with $choice of $measure cm.")}
      else {
        println(s"\n\n** I'm sorry, there is no data to print for $measure cm $choice **")
      }
      println(s"Displaying $newdf rows of data. ")
    }


    println("\n\nEnd of Program -------------------------------------------------------------------------\n")


  }


}
